package models;

public class Date {
    // khai báo các thuộc tính.. và cho chúng mặc định là
    private int day = 10;
    private int month = 04;
    private int year = 2023;

    // khởi tạo với đầy đủ 3 tham số đầu vào
    public Date(int day, int month, int year) {
        this.day = day;
        this.month = month;
        this.year = year;
    }

    public int getDay() {
        return day;
    }

    public int getMonth() {
        return month;
    }

    public int getYear() {
        return year;
    }

    public void setDay(int day) {
        this.day = day;
    }

    public void setMonth(int month) {
        this.month = month;
    }

    public void setYear(int year) {
        this.year = year;
    }
    public void setDate(int day, int month, int year) {
        this.year = year;
        this.month = month;
        this.day = day;
    }

    
    public String toString() {
        return "Date [day=" + day + ", month=" + month + ", year=" + year + "]";
    }
    
    

   
}
